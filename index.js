const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");

const app = express();

// Connect to MongoDB
mongoose.connect("mongodb+srv://dbalfredmanalang:MttlylLvtpD7OACh@wdc028-course-booking.qczdh.mongodb.net/ecommerceDB?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once('open', () => console.log('Now connected to MongoDBAtlas'))

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file (ADDED IN SECOND PHASE)
app.use("/users", userRoutes);

app.use("/products", productRoutes);

app.use("/", orderRoutes);


// Listening to port
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process. env.PORT || 4000 }`)
});