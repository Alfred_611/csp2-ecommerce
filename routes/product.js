// PRODUCT ROUTES
const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// ROUTE FOR CREATE PRODUCT
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	productController.addProduct(userData, req.body).then(resultFromController => res.send(resultFromController));
});


// ROUTE FOR RETRIEVE ALL ACTIVE PRODUCTS
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// ROUTE FOR RETRIEVE A SPECIFIC PRODUCT
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// ROUTE FOR UPDATING A PRODUCT
router.put("/:productId", auth.verify, (req, res) => {
	
	const user = auth.decode(req.headers.authorization)
	productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// ROUTE FOR ARCHIVE A PRODUCT
router.put("/:productId/archive", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.archivedProduct(req.params, req.body, user).then(resultFromController => res.send(resultFromController));
});

module.exports = router;