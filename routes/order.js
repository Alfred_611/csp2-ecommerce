// ORDER ROUTES
const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth");


//1. ROUTE TO ORDER A PRODUCT (A NON-ADMIN ONLY FEATURE)
router.post("/buy", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	orderController.buyProduct(userData, req.body).then(resultFromController => res.send(resultFromController));
});



//2. ROUTE TO RETRIEVE ALL ORDERS (AN ADMIN ONLY FEATURE)
router.get("/users/orders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	orderController.retrieveAllOrders(userData).then(resultFromController => res.send(resultFromController));
});



//3. ROUTE TO RETRIEVE AUTHENTICATED USER'S ORDERS (A USER FEATURE)



// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;