 // USER MODEL
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required: [true, "A first name is required"]
	},

	lastName : {
		type: String,
		required: [true, "A last name is required"]
	},

	email : {
		type: String,
		required: [true, "An email is required"]
	},

	password : {
		type: String,
		required: [true, "A password is required"]
	},
	
	isAdmin : {
		type : Boolean,
		required : false
	},

	Orders: [{
		orderId: {
			type: String
	}
	}]
});

module.exports = mongoose.model("User", userSchema);