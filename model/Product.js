// PRODUCT MODEL
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	
	name : {
		type: String,
		required: [true, "A name is required"]
	},
	description : {
		type: String,
		required: [true, "A description is required"]
	},
	price : {
		type : Number,
		required : [true, "A price is required"]
	},

	isActive : {
		type : Boolean,
		default: true
	},

	createdOn : {
		type: Date,
		default: new Date()
	},

	thoseWhoOrdered : [{
		orderId : {
			type: String
	}
	}]
});

module.exports = mongoose.model("Product", productSchema);