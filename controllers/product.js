// PRODUCT CONTROLLER
const Product = require("../model/Product");
const User = require("../model/User");
const Order = require("../model/Order");

// CREATE A PRODUCT (AN ADMIN ONLY FEATURE)
module.exports.addProduct = async (userData, reqBody) => {
	if(userData.isAdmin){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive,
			createdOn: reqBody.createdOn,
			thoseWhoOrdered: reqBody.thoseWhoOrdered
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return false;
	}
};



// RETRIEVE ALL ACTIVE PRODUCTS 
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
};



// RETRIEVE A SPECIFIC PRODUCT
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};



// UPDATE A PRODUCT
module.exports.updateProduct = (user, reqParams, reqBody) => {
	// Validate if user is Admin
	if (user.isAdmin){
		//Specify the fields/properties of the document to be updated
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}
		else{
				return true;
			}
		})
	}else{
		return false;
	}
};



// ARCHIVE A PRODUCT
module.exports.archivedProduct = async (reqParams, reqBody, user) => {
	if(user.isAdmin){
		return Product.findById(reqParams.productId).then(result => {
			result.isActive = false;

			return result.save().then((product, error) => {
				if(error){
					return false;
				} else {
					return `The product/item has been archived.`;
				}
			})
		});

	} else {
		return false;
	}
};