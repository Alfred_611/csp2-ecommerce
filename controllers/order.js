// ORDER CONTROLLER
const Order = require("../model/Order");
const User = require("../model/User");
const Product = require("../model/Product");

// REGISTER ORDER
module.exports.registerOrder = async (userData, data) => {
	if(userData.isAdmin === false){
	// Add the user ID in the order
	let isOrderUpdated = await Order.findById(data.orderId).then(order => {
		// Adds the userId in the orders array
		order.userId.push({userId : data.userId});

		// Saves the updated order information in the database
		return order.save().then((order, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	// Condition that will check if the user and product documents have been updated
		if(isOrderUpdated){
			return true;
		}
		else{
			return false;
		}
}else{
		return `You are an admin and therefore cannot order a product.`;
	}

};


//1. 





//2. RETRIEVE ALL ORDERS (AN ADMIN ONLY FEATURE)
module.exports.retrieveAllOrders = async (userData) => {
	if(userData.isAdmin){
	return Order.find({}).then(result => {
			// Returns the user information with the password as an empty string
			return result;
		})
	}
else{
	return `You are not authorized to do this request.`;
}
};


//3. RETRIEVE AUTHENTICATED USER'S ORDERS
module.exports.getMyOrders = async (userData) => {
	if(userData.isAdmin == false) {
		return Order.findOne({customerId: userData.id}).then(result => {
			if (result == null){
				return "You have no items in your cart."
			}
			else{
				return result;
			}
		})
	}
	else{
		return "Please login."
	}
}