// USER CONTROLLER
const User = require("../model/User"); //need ilink sila model
const Product = require("../model/Product");
const Order = require("../model/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth"); 

// REGISTER USER
module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		isAdmin : reqBody.isAdmin,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// GET SPECIFIC USER DETAILS
module.exports.retrieveUser = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// LOGIN REQUEST & GENERATE TOKEN
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return `email is incorrect/incomplete.`;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }

			}	
			else{
				return `password is incorrect.`;
			}
		}
	})
}

// ADMIN REGISTRATION (AN ADMIN ONLY FEATURE)
module.exports.setAdmin = async (userData, reqParams) => {
	if(userData.isAdmin){
		return User.findByIdAndUpdate({_id: reqParams.userId}, {isAdmin: true}).then(result =>{
			return result.save().then((updatedResult, error) =>{
				if(error){
					return `Failed to process request.`;
				} else{
					return `Successfully updated.`;
				}
			})
		})
	} else{
		return `You are not authorized to do this operation.`;
	}
}

// ORDER A PRODUCT
// Async await will be used in ordering a product because we will need to update two separate documents when buying a product:
module.exports.buyProduct = async (userData, reqBody) => {
	if(userData.isAdmin == false){
	// Add the order ID in the orders array of the user 
		let isUserUpdated = await User.findById(data.userId).then(user => {
			// Adds the orderId in the user's orders array
			user.Orders.push({orderId : data.orderId});

			// Saves the updated user information in the database
			return user.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		// Add the order ID in the thoseWhoOrdered array of the product
		let isProductUpdated = await Product.findById(data.productId).then(product => {
			// Adds the orderId in the product's thoseWhoOrdered array
			product.thoseWhoOrdered.push({orderId : data.orderId});

			// Saves the updated product information in the database
			return product.save().then((product, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		// Condition that will check if the user and product documents have been updated
		if(isUserUpdated && isProductUpdated){
			return true;
		}
		else{
			return false;
		}
	}else{
		return `You are an admin and therefore cannot order a product.`;
	}
};


